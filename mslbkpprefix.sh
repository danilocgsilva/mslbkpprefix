#!/bin/bash

## version
VERSION="0.0.1"

## Asks if the user want to utilizes the login-path or provides crdentials
asks () {
  echo "You will provides the login-path value or the database credentials?"
  read -p "Writes \"login-path\" or \"credentials\": " login_path_or_credential
  if [[ $login_path_or_credential =~ (credentials|login-path) ]]
  then
    echo ok!
  else
    echo Incorrect value!
    asks
  fi
}

## Main function
mslbkpprefix () {
  local login_path_or_credential
  asks
  
}

## detect if being sourced and
## export if so else execute
## main function with args
if [[ /usr/local/bin/shellutil != /usr/local/bin/shellutil ]]; then
  export -f mslbkpprefix
else
  mslbkpprefix "${@}"
  exit 0
fi
